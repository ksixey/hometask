document.addEventListener('DOMContentLoaded',()=>{
        let color =localStorage.getItem('bgcolor');
        if(color) document.querySelector('.bg-body').style.backgroundColor =color;
        let button = document.querySelector('.click-button');
        button.addEventListener('click',onClick);
        function onClick(){
                if(document.querySelector('.bg-body').style.backgroundColor==='lightblue'){
                        document.querySelector('.bg-body').style.backgroundColor=''
                        localStorage.setItem('bgcolor','')
                }else{
                        document.querySelector('.bg-body').style.backgroundColor='lightblue'
                        localStorage.setItem('bgcolor','lightblue')
                }
        }
})