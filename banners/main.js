let img = document.querySelectorAll('.images-wrapper .image-to-show')
let current =0;
let intervalTime = setInterval(onInterval,2000);
let isActive;
function onInterval() {
    img.forEach((item,i)=>{
        img[i].classList.add('opacity0')
    })
    img[current].classList.remove('opacity0');
    if(current+1 === img.length){
        current=0
    }else{
        current++;
    }
}
let stop = document.querySelector('.stop')
stop.addEventListener('click',stopSlider)

function stopSlider() {
    clearInterval(intervalTime)
    isActive =true;
}

let continueSlide = document.querySelector('.continue');
continueSlide.addEventListener('click',continueSliders);
function continueSliders() {
    if (isActive) {
        isActive=false;
        clearInterval(intervalTime)
        intervalTime = setInterval(onInterval,2000);
    }
}
