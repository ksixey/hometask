// При загрузке страницы показать пользователю поле ввода (input) с надписью Price.
// Это поле будет служить для ввода числовых значений
// Поведение поля должно быть следующим:
//
//     При фокусе на поле ввода - у него должна появиться рамка зеленого цвета.
//     При потере фокуса она пропадает.
//     Когда убран фокус с поля - его значение считывается, над полем создается span,
//     в котором должен быть выведен текст: Текущая цена: ${значение из поля ввода}.
//     Рядом с ним должна быть кнопка с крестиком (X). Значение внутри поля ввода окрашивается в зеленый цвет.
//     При нажатии на Х - span с текстом и кнопка X должны быть удалены. Значение, введенное в поле ввода,
//     обнуляется.
//     Если пользователь ввел число меньше 0 - при потере фокуса подсвечивать поле ввода красной рамкой, под
//     полем выводить фразу - Please enter correct price. span со значением при этом не создается.
//
//
//     В папке img лежат примеры реализации поля ввода и создающегося span.

let money =document.querySelector('#money');
let cost = document.createElement('span');
let close =document.createElement('span');
let label =document.querySelector('.label-div');
let attention = document.createElement('div');
money.addEventListener('focus',onFocus);
money.addEventListener('blur',onBlur);

function onFocus(event) {
  event.target.style.outline = '0px';
  event.target.style.borderColor = 'green';
}
function onBlur(event) {
    event.target.style.borderColor = '';
    if(money.value>0) {
        cost.className = `costs-style`
        cost.innerHTML = `Твой счет: ${money.value} $`;
        money.style.color = 'green';
        label.before(cost);
        close.className = `close`;
        close.innerHTML = `x`;
        cost.append(close);
        attention.remove();

    }
    else{
        money.style.color = 'red';
        money.after(attention);
        attention.innerHTML = `Please enter correct price`;
        cost.remove()
    }
}

close.addEventListener('click',deleteSpan)
function deleteSpan(){
        money.value= '';
        money.style.color='black';
        cost.remove()
}

