$('#menu').on('click','.nav-link',function (event) {
        event.preventDefault();
        let id =$(this).attr('href');
        let top =$(id).offset().top;
    })

$(window).scroll(onScroll);
function onScroll() {
    const buttonScroll ='scroll-top'
    if (window.scrollY > window.innerHeight){
        if(!$(`.${buttonScroll}`).length){
            $('<button>Go up!</button>')
                .css({
                    position: 'fixed',
                    right: 10,
                    bottom: 50+'%',
                    background :'#90c6cc',
                    border:'none',
                    padding: 10,
                    color:'#FFE9E6'
                })
                .addClass(buttonScroll)
                .click(scrollTop)
                .appendTo(document.body)
        }
    }else{
        $(`.${buttonScroll}`).remove()
    }
}
function scrollTop() {
    $('html,body').animate({scrollTop:0},2000)
}

$("#close").click(function(){
    $("#news").slideToggle(3000);
});


